import time
import os
import random
import pygame 
from moviepy.editor import VideoFileClip

PAUSE, BACK, FORWARD = 1,2,3
BLACK = (0, 0, 0) 

class MovieDisplay:
    def __init__(self):
        self.X = 1920
        self.Y = 1080
        self.movieTime = 30
        self.pictureTime = 3
        self.fullscreen = True
        self.movieFormats = {"mp4", "MOV", "AVI", "MP4", "avi", "3GP", "mov", "gif", "flv"}
        self.folder = "/media/oscar/14EA-A4E5"
        self.lookUp = dict()
        self.order = []
        self.numPictures = 0
        self.init()

    def init(self):
        pygame.init() 
        randSet = set()
        for entry in os.listdir(self.folder):
            if entry != ".Trash-1000" and entry != "System Volume Information":
                for picture in os.listdir(self.folder+"/"+entry):
                    for p in os.listdir(self.folder+"/"+entry+"/"+picture):
                        self.lookUp[self.numPictures] = entry+"/"+picture+"/"+p
                        randSet.add(self.numPictures)
                        self.numPictures += 1
        self.order = random.sample(randSet, self.numPictures)

    def showMovie(self, path):
        clip = VideoFileClip(path)
        clip = clip.subclip(0,min(clip.duration, self.movieTime))
        clip = clip.resize((self.X, self.Y))
        clip.preview(fullscreen=self.fullscreen)
        pygame.mouse.set_pos((self.X,self.Y))

    def showPicture(self, path):
        file = open(path)
        image = pygame.image.load(file) 
        imageBox = image.get_rect()
        scale = imageBox.w/imageBox.h
        if(scale < self.X/self.Y):
            image = pygame.transform.scale(image, (int(self.Y*scale), self.Y))
            dx = (self.X - int(self.Y*scale))/2
            return image, (dx,0)
        else:
            image = pygame.transform.scale(image, (self.X, int(self.X/scale)))
            dy = (self.Y - int(self.X/scale))/2
            return image, (0, dy)

        file.close()

    def waitForEvent(self, waitTime):
        timeToReturn = time.time() + waitTime
        d = {" ": PAUSE, "a": BACK, "d": FORWARD}
        while(time.time() < timeToReturn):
            for event in pygame.event.get():
                if event.type == pygame.QUIT or event.type == 5:
                    return False, -1
                elif event.type == 2:
                    return True, d.get(event.unicode, -1)
            time.sleep(0.1)
        return True, -1
        
    def run(self):
        screen = pygame.display.set_mode((self.X, self.Y), pygame.FULLSCREEN)
        currentPicture = 0
        paused = False
        running = True
        while running:
            path = self.folder + "/" + self.lookUp[self.order[currentPicture%self.numPictures]]
            filetype = path.split(".")[-1]
            print(path)

            screen.fill(BLACK)  

            if filetype in self.movieFormats:
                self.showMovie(path)
                running, command = self.waitForEvent(0.1)
                pygame.mouse.set_pos((self.X,self.Y))

            else:
                image, pos = self.showPicture(path)
                screen.blit(image, pos) 
                if paused:
                    pygame.draw.circle(screen, (255,0,0), (10,10), 10)
                pygame.display.update()  
                running, command = self.waitForEvent(self.pictureTime)

            if command == BACK:
                currentPicture -= 1
            elif command == PAUSE:
                paused = not paused
            elif command == FORWARD or not paused:
                currentPicture += 1


        pygame.quit() 

m = MovieDisplay()
m.run()

